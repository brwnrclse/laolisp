;;;; Guess My Number game!

(defparameter *floor* 1)
(defparameter *ceiling* 100)

(defun guess-my-number ()
  (ash (+ *floor* *ceiling*) -1))

(defun smaller ()
  (setf *ceiling* (1- (guess-my-number)))
  (guess-my-number))

(defun bigger ()
  (setf *floor* (1+ (guess-my-number)))
  (guess-my-number))

(defun start-over ()
  (defparameter *floor* 1)
  (defparameter *ceiling* 100)
  (guess-my-number))
