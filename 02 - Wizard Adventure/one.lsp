;;;; Wizard Adventure text game engine

(defparameter *nodes* '((wizard-room (you are in the wizard-room.
                                           a wizard is a snoring loudly on the toilet))
                         (garden (you are in a beautiful garden.
                                      there is a well in front of you.))
                         (attic (you are in the attic.
                                     there is a talking owl perched on the window.)))
  "alist for game locations and their descriptions")

(defparameter *edges* '((wizard-room (garden west door)
                                     (attic upstairs "dangling rope"))
                        (garden (wizard-room east door))
                        (attic (wizard-room downstairs "dangling room")))
  "Documentation for edges")

(defun describe-location (location nodes)
  "A quick lookup function for our location nodes"
  (cadr (assoc location nodes)))

(defun describe-path (edge)
  "A quick lookup function for the paths connecting each node"
  '(there is a ,(caddr edge) going ,(cadr edge) from here.))
(defun describe-paths (location edges)
  "Lookup for multiple paths a once"
  (apple #'append (mapcar #'describe-path (cdr (assoc location edges)))))
